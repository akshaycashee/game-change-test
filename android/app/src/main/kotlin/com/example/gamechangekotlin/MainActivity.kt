package com.example.gamechangekotlin


import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.gamechangesns.nboard.GCOnBoarding.init
import com.gamechangesns.nboard.GCOnBoarding.readEmiratesIDBack
import com.gamechangesns.nboard.listeners.CompleteEIDScanResultListener
import com.gamechangesns.nboard.models.CompleteEIDCard
import com.gc.logging.listeners.GCSdkInitCallback
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity: FlutterActivity() {

    private val CHANNEL = "gamechannel"

    @RequiresApi(Build.VERSION_CODES.M)
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
                call, result ->
            if (call.method == "gcinitmethod") {

                init(this, object : GCSdkInitCallback {
                    override fun onLicenseValid() {
                        result.success("Licence Valid")
                    }

                    override fun onError(i: Int) {
                        result.error("$i", "Error", "licence not valid")
                    }
                })

            } else if (call.method == "cameraPermissionMethod") {

                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                ) {
                    val listPermissionsNeeded = arrayOf(Manifest.permission.CAMERA)
                    ActivityCompat.requestPermissions(this@MainActivity, listPermissionsNeeded, 1)
                    //requestPermissions(new String[]{Manifest.permission.CAMERA}, 200);
                    Toast.makeText(
                        this@MainActivity,
                        "Please grant camera permission",
                        Toast.LENGTH_SHORT
                    ).show()
                    result.error("100", "Camera permission not granted", "No camera permission")

                } else {
                    result.success("Camera Permission granted")
                }

            } else if (call.method == "scanBackIdMethod") {

                readEmiratesIDBack(this@MainActivity, object : CompleteEIDScanResultListener {
                    override fun onCardReadSuccess(completeEIDCard: CompleteEIDCard?) {
                        result.success(
                            "${completeEIDCard!!.getCardNumberBackSide()} ${ completeEIDCard!!.getDobBackSideYYMMDD()} ${completeEIDCard!!.getExpiryBackSideYYMMDD()} "
                        )
                    }

                    override fun onCardReadError(i: Int) {
                        result.error("101", "Card scan failed", "Card scan failed")
                    }
                }, 15)




            }

            else {
                result.notImplemented()
            }
        }
    }
}
