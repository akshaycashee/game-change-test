import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gamechangekotlin/sample_bloc.dart';

class SampleScreen extends StatefulWidget {
  const SampleScreen({Key? key}) : super(key: key);

  @override
  _SampleScreenState createState() => _SampleScreenState();
}

class _SampleScreenState extends State<SampleScreen> {
  late SampleBloc _sampleBloc;

  @override
  void initState() {
    super.initState();
    _sampleBloc = SampleBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SampleBloc>(
      create: (final BuildContext context) {
        return _sampleBloc;
      },
      child: BlocBuilder<SampleBloc, SampleBlocState>(
        builder: (
          final BuildContext context,
          final SampleBlocState state,
        ) {
          return Scaffold(
            appBar: AppBar(),
            body: _body(state),
          );
        },
      ),
    );
  }

  Widget _body(final SampleBlocState state) {
    if (state is SampleBlocLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              _sampleBloc.add(InitGC());
            },
            child: const Text("Init GC"),
          ),
          const SizedBox(
            height: 18,
          ),
          ElevatedButton(
            onPressed: () {
              _sampleBloc.add(CameraPermission());
            },
            child: const Text("Camera Permission"),
          ),
          const SizedBox(
            height: 18,
          ),
          ElevatedButton(
            onPressed: () {
              _sampleBloc.add(ScanBackId());
            },
            child: const Text("Scan Back ID"),
          ),
          const SizedBox(
            height: 18,
          ),
          ElevatedButton(
            onPressed: () {
              _sampleBloc.add(NFCPermission());
            },
            child: const Text("NFC Permission"),
          ),
          const SizedBox(
            height: 18,
          ),
          ElevatedButton(
            onPressed: () {
              _sampleBloc.add(ReadNFC());
            },
            child: const Text("Read NFC"),
          ),
          const SizedBox(
            height: 18,
          ),
        ],
      ),
    );
  }
}
