// Event
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class SampleBlocEvent {}

class InitGC extends SampleBlocEvent {}

class CameraPermission extends SampleBlocEvent {}

class ScanBackId extends SampleBlocEvent {}

class NFCPermission extends SampleBlocEvent {}

class ReadNFC extends SampleBlocEvent {}

// State
abstract class SampleBlocState {}

class SampleBlocLoading extends SampleBlocState {}

class SampleBlocLoaded extends SampleBlocState {}

// Bloc
class SampleBloc extends Bloc<SampleBlocEvent, SampleBlocState> {
  SampleBloc() : super(SampleBlocLoaded());

  final methodChannel = const MethodChannel('gamechannel');
  final initGCMethod = 'gcinitmethod';
  final cameraPermissionMethod = 'cameraPermissionMethod';
  final scanBackIdMethod = 'scanBackIdMethod';

  @override
  Stream<SampleBlocState> mapEventToState(SampleBlocEvent event) async* {
    if (event is InitGC) {
      yield SampleBlocLoading();

      String response = "";
      try {
        final String result = await methodChannel.invokeMethod(initGCMethod);
        response = result;
      } on PlatformException catch (e) {
        response = "Failed to Invoke: '${e.message}'.";
      }

      print(response);

      yield SampleBlocLoaded();
    } else if (event is CameraPermission) {
      yield SampleBlocLoading();

      String response = "";
      try {
        final String result =
            await methodChannel.invokeMethod(cameraPermissionMethod);
        response = result;
      } on PlatformException catch (e) {
        response = "Failed to Invoke: '${e.message}'.";
      }

      print(response);

      yield SampleBlocLoaded();
    } else if (event is ScanBackId) {
      yield SampleBlocLoading();

      String response = "";
      try {
        final String result =
            await methodChannel.invokeMethod(scanBackIdMethod);
        response = result;
      } on PlatformException catch (e) {
        response = "Failed to Invoke: '${e.message}'.";
      }

      print(response);

      yield SampleBlocLoaded();
    } else if (event is NFCPermission) {
      yield SampleBlocLoading();

      yield SampleBlocLoaded();
    } else if (event is ReadNFC) {
      yield SampleBlocLoading();

      yield SampleBlocLoaded();
    }
  }
}
